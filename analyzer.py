#!/usr/bin/python

import sys
import json
from datetime import datetime
import matplotlib.pyplot as plt

title = ''

class Entry(object):
	"""
	Entry class represents each entry.
	"""

	def __init__(self,arrayObj):
		super(Entry, self).__init__()
		self.isDay = True if arrayObj['ISDAY'] == "1" else False
		startDateTime = datetime.fromtimestamp(int(arrayObj['STARTTIME'])/1e3)
		self.startTime = startDateTime - datetime(
			startDateTime.year,
			startDateTime.month,
			startDateTime.day)
		self.startTimeMinutes = self.startTime.seconds/60
		endDateTime = datetime.fromtimestamp(int(arrayObj['ENDTIME'])/1e3)
		self.endTime = endDateTime - datetime(
			endDateTime.year,
			endDateTime.month,
			endDateTime.day)
		self.endTimeMinutes = self.endTime.seconds/60
		self.duration = self.endTime - self.startTime
		self.id = int(arrayObj['ID'])
	
	def checkIsDay(self):
		return True if self.isDay else False


def fetchData(filename):
	global title
	try:
		fileString = open(filename).read()
	except Exception as e:
		raise e
	json_data = json.loads(fileString)
	list_data = [Entry(x) for x in json_data]
	title = filename.split('.')
	return list_data

def displayGraph(data):
	plt.plot([x.startTimeMinutes for x in data], [1 for x in data], 'rx')
	plt.title(title[0])
	plt.xlabel('Minutes from 0000 hour.')
	plt.ylabel('Device Usage State')
	plt.grid(True)
	plt.show()

def main():
	try:
		# Checks if the file name is appended as argument.
		filename = sys.argv[1]
		# Fetch data from the file.
		data = fetchData(filename)
		# Displays a graph.
		displayGraph(data)
		# Find all the day entries.
		day = [x for x in data if x.checkIsDay()]
		# Find all the night entries.
		night = [x for x in data if not x.checkIsDay()]
		print('Day entry size:', len(day))
		print('Night entry size:', len(night))
	except IndexError as e:
		print('Filename argument is empty.')
	except Exception as e:
		print(e)

if __name__ == "__main__":
	main()